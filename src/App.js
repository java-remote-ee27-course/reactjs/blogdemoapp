import { useState } from "react";
import "./App.css";

function App() {
  return (
    <div className="App">
      <Blog />
    </div>
  );
}

function Blog() {
  return (
    <div>
      <BlogPost />
    </div>
  );
}

function BlogPost() {
  const [photo, setPhoto] = useState();
  const [draftText, setDraftText] = useState("");
  const [blogText, setBlogText] = useState("");
  const [draftHeader, setDraftHeader] = useState("");
  const [header, setHeader] = useState("");
  const [comments, setComments] = useState([]);

  const draftTextPlaceHolder = "Write your post here";
  const draftCommentPlaceHolder = "Write your comment here";

  function submitBlogText() {
    setBlogText(draftText);
    setHeader(draftHeader);
    setDraftText("");
    setDraftHeader("");
  }

  function handleAddComment(draftComment) {
    setComments((comments) => [...comments, draftComment]);
  }

  return (
    <>
      <Photo photo={photo} photoWrapper="post-photo-wrapper" />
      <div className="card">
        <div className="card post-card">
          <Header header={header} generalHeader="post-header"></Header>
          {blogText.length > 0 ? (
            <div className="post-text-container">
              <TextContent
                textContent={blogText}
                wrapper="posttext-wrapper"
                contentWrapper="posttext"
              />
            </div>
          ) : (
            <div className="post-text-empty-container">
              Let's start writing something!
            </div>
          )}
          {comments.length > 0 && (
            <CommentList comments={comments}>Comments:</CommentList>
          )}
          {blogText.length > 0 && (
            <CommentForm
              commentUploader="comment-uploader"
              draftCommentPlaceHolder={draftCommentPlaceHolder}
              onAddComment={handleAddComment}
            ></CommentForm>
          )}
        </div>
        <div className="card draft-card">
          <Header generalHeader="draft-main-header">Write a blogpost</Header>
          <FileUploader onSetPhoto={setPhoto} btnUpload="btn-upload" />
          <DraftHeader
            text={draftHeader}
            onSetDraftHeader={setDraftHeader}
            placeholder="Your post headline"
          />
          <DraftText
            draftText={draftText}
            onSetDraftText={setDraftText}
            draftPlaceHolder={draftTextPlaceHolder}
            textareaStyling="text-uploader"
          />
          <Button onSubmit={submitBlogText} buttonStyle="btn-primary">
            Publish post
          </Button>
        </div>
      </div>
    </>
  );
}

function CommentForm({ onAddComment }) {
  const [draftComment, setDraftComment] = useState("");
  function handleSubmit(event) {
    event.preventDefault();
    const newComment = { comment: draftComment, id: Date.now() };
    onAddComment(newComment);
    setDraftComment("");
  }
  return (
    <>
      <form className="add-form" onSubmit={handleSubmit}>
        <DraftComment
          draftComment={draftComment}
          onSetDraftComment={setDraftComment}
          textareaStyling="comment-uploader"
          draftTextWrapper="textarea-wrapper"
          draftPlaceHolder="Write your text here"
        >
          Leave your comment:
        </DraftComment>
        <Button buttonStyle="btn-primary">Publish comment</Button>
      </form>
    </>
  );
}
function Photo({ photo, photoWrapper }) {
  return (
    <div className={photoWrapper}>
      {!photo ? (
        <img
          src="./assets/img_placeholder.png"
          alt="placeholder"
          className="photo-placeholder"
        />
      ) : (
        <img src={photo} alt="" className="post-photo" />
      )}
    </div>
  );
}

function FileUploader({ photo, onSetPhoto, btnUpload }) {
  function handleChange(e) {
    if (e.target.files.length !== 0) {
      const urlObject = URL.createObjectURL(e.target.files[0]);
      onSetPhoto(urlObject);
    }
  }

  return (
    <div className="post-coverphoto-uploader">
      <input
        type="file"
        value={photo}
        onChange={handleChange}
        className={btnUpload}
      />
    </div>
  );
}

function Header({ header, children, generalHeader }) {
  return (
    <h1 className={generalHeader}>
      {header} {children}
    </h1>
  );
}
function Comment({ comment }) {
  return (
    <div>
      <div value={comment}>{comment}</div>
    </div>
  );
}
function CommentList({ children, comments }) {
  return (
    <div className="comment-list">
      <h4>{children}</h4>
      {comments.map((comment) => (
        <Comment
          key={comment.id}
          comment={comment.comment}
          wrapper="comment-wrapper"
          contentWrapper="comment"
        ></Comment>
      ))}
    </div>
  );
}
function DraftComment({
  draftTextWrapper,
  textareaStyling,
  draftPlaceHolder,
  draftComment,
  onSetDraftComment,
  children,
}) {
  return (
    <div className={draftTextWrapper}>
      <h4>{children}</h4>
      <textarea
        className={textareaStyling}
        placeholder={draftPlaceHolder}
        onChange={(e) => onSetDraftComment(e.target.value)}
        value={draftComment}
      >
        {draftComment}
      </textarea>
    </div>
  );
}

function TextContent({ textContent, wrapper, contentWrapper, children }) {
  return (
    <div className={wrapper}>
      <h3>{children}</h3>
      <div className={contentWrapper}>{textContent}</div>
    </div>
  );
}

function DraftHeader({ text, onSetDraftHeader, placeholder }) {
  return (
    <div>
      <input
        type="text"
        value={text}
        placeholder={placeholder}
        onChange={(e) => onSetDraftHeader(e.target.value)}
        className="draft-header"
      />
    </div>
  );
}

function DraftText({
  draftText,
  onSetDraftText,
  draftPlaceHolder,
  draftTextWrapper,
  textareaStyling,
  children,
}) {
  return (
    <div className={draftTextWrapper}>
      {children}
      <textarea
        className={textareaStyling}
        placeholder={draftPlaceHolder}
        value={draftText}
        onChange={(e) => onSetDraftText(e.target.value)}
      >
        {draftText}
      </textarea>
    </div>
  );
}
function Button({ children, onSubmit, buttonStyle }) {
  return (
    <div>
      <button onClick={onSubmit} className={buttonStyle}>
        {children}
      </button>
    </div>
  );
}

export default App;

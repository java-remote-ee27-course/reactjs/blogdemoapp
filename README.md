# Blog Demo Frontend Mockup

Simple blog demo frontend created in ReactJS.

- Write a post and "publish" it
- Add comments (with form)
- Conditional rendering of comments list and comments area
- "Upload" picture
- Re-usable components

## To-do

- Gaps between comments
- Authors of comments
- Creation time
- Backend

![blog demo](./public/assets/blog6.png)
![blog demo](./public/assets/blog1.png)
![blog demo](./public/assets/blog7.png)

**Initial version:**

![blog demo](./public/assets/blog2.png)

## Created by

Katlin
